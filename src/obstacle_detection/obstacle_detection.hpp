#pragma once

#include <dynamic_reconfigure/server.h>
#include <ros/forwards.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>

#include <Eigen/Eigen>
#include <Eigen/Dense>

#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl_ros/point_cloud.h>

#include <opencv/cv.hpp>
#include <cv_bridge/cv_bridge.h>

#include <sensor_msgs/Image.h>

#include "neverdrive_obstacle_detection_ros_tool/ObstacleDetectionInterface.h"

using namespace std;
using namespace cv;

namespace neverdrive_obstacle_detection_ros_tool {

class ObstacleDetection {

    using Interface = ObstacleDetectionInterface;

    using Msg = sensor_msgs::Image;

public:
    ObstacleDetection(ros::NodeHandle, ros::NodeHandle);

private:
    void callbackSubscriber(const Msg::ConstPtr& msg);
    void reconfigureRequest(const Interface::Config&, uint32_t);
    /*****/

    void maskDepth(cv::Mat & ground, cv::Mat &,cv::Mat &, int, int );
    vector<vector<Point> > find_obstacles(cv::Mat &image, int minThre , int maxThre , int area );
    bool pixelInMapArea(cv::Mat & image,int v,int u);
    void getIr2World();

    void depthImageCallback(const Msg::ConstPtr& msg);
    void depthImageCallback(const ros::TimerEvent&);
    void readGroundMap(const std::string&);

    cv::Mat ground_depth_;
    Eigen::Affine3d ir_2_world_;
    ros::Subscriber depth_image_subscriber_;
    ros::Publisher depth_image_publisher_;
    ros::Timer depth_image_pub_timer_;
    int count_ =0;
    const int ground_threshold_ = 100;


    /*****/
    Interface interface_;
    dynamic_reconfigure::Server<Interface::Config> reconfigureServer_;

    tf2_ros::Buffer tfBuffer_;
    tf2_ros::TransformListener tfListener_{tfBuffer_};
    tf2_ros::TransformBroadcaster tfBroadcaster_;
};
} // namespace neverdrive_obstacle_detection_ros_tool
